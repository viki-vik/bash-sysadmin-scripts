#!/usr/bin/env bash
#set -x
##########################################################################
#created by: Vika Baktov
#purpose: recieve file, provide write permission if current user its owner
#date: 24/10/2020
#version: 1.0.0
##########################################################################

main(){
	local _file=$1
	local _permission_to_set="w"	
	if [[ -z $_file ]];then
		read -p  "You have to give a file: " _file
	fi
	
	if [[ "$(file_exist $_file)"="exists" ]]&&[[ "$(check_owner $_file)"="owner" ]];then		
		set_permission $_file "$(file_permission $_file)" $_permission_to_set
	else
		help
	fi
}

file_exist(){
	local _file=$1
	if [[ -f "$_file" ]];then
		deco "$_file file exists"
		echo "exists"
	else
		deco "$_file file is not exist"
		help
	fi
}

file_owner(){
	local _file=$1
	echo $(ls -lh $_file| cut -d' ' -f3)
}

check_owner(){
	local _file=$1
	if [[ $(file_owner $_file)!=$(whoami) ]];then
		deco "You are not a file owner"
		#echo "not owner"
	else
		#deco "You $_f_owner are owner of: $_file"
		echo "owner"
	fi
}

file_permission(){
	local _file=$1 
	local _permission=$(ls -lh $_file| cut -d' ' -f1)
	echo ${_permission:2:1}
}

set_permission(){
	local _file=$1
	local _permission=$2
	local _attribute=$3
	echo $_file
	echo $_permission
	echo $_attribute
	if [[ $_permission!=$_attribute ]];then 
		chmod u+"$_attribute" $_file
		deco "Provided user permission: $_attribute"
	else
		deco "User already has permission: $_attribute"
	fi	
}

deco(){
	_time=2.5
	l="##############################"
	printf "\n$l\n # %s\n$l\n" "$@"
	sleep $_time
	#clear
}

help(){
	deco "Incorrect use of script"
	exit 1
}
#################### !!!!! DO NOT REMOVE !!!!!!!! ###########
main "$@"

