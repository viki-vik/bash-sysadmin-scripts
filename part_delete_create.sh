#!/usr/bin/env bash 
#set -x

# fdisk 2 functions : delete_parts create_part
# mkfs 1 function: create_fs_on_disk
# mount 1 function, at least  2 params:  mount_disk_on_dest
# misc: deco, help, main

main(){
	count=0
	local disk_name="$1"
	local disk_size="$2"
	
	if [[ -z $disk_name ]];then
		help
	else
		parts=$(check_amount_of_parttions $disk_name)
		while [[ $parts -gt count ]]
		do
			delete_parts $disk_name
			let count++
		done
		
		deco "Finished Deleting Partitions"
		
		create_part $disk_name $disk_size
		
		deco "Finished Creating Partition"
		
		create_fs_on_disk_and_mount $disk_name
		
		deco "Created FS and mounted on /mnt$disk_name"
	fi
	}

check_amount_of_parttions(){
local disk_name="$1"
if [[ -z $disk_name ]];then 
    help    
    exit 1  
else
    amount_of_partitions=$(fdisk  -l $disk_name|grep  -A100 "Device"|wc -l)
   echo $amount_of_partitions
fi
}

create_fs_on_disk_and_mount(){
	local disk_name="$@"
		
	if [[ -z $disk_name ]];then
		help
	else
		disk_part=$(fdisk  -l $disk_name|awk '{print $1}'|grep '/dev')
		mkfs.ext4 $disk_part
		sleep $_time
		disk_part=${disk_part:5}
		mkdir -p /mnt/$disk_part
		mount /dev/$disk_part /mnt/$disk_part
	
	fi
	}
	

create_part(){
	
local disk_name="$1"
local disk_size="$2"
if [[ -z $disk_name ]] && [[ -z $disk_size ]];then
	help
else
fdisk $disk_name << EOF
n
p


+$disk_size	
w	
EOF

fi

}


delete_parts(){
#scope
local disk_name="$@"
if [[ -z $disk_name ]];then
	help
else

fdisk $disk_name << EOF
d
	
w	
EOF

fi

}


deco(){
	_time=2.5
	l="##############################"
	printf "\n$l\n # %s\n$l\n" "$@"
	sleep $_time
	clear
}

help(){
	deco "Incorrect use of script"
	exit 1
}

##################### !!!!! DO NOT REMOVE !!!!!!!! ###########
main "$@"
