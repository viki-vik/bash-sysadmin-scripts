#!usr/bin/env bash
#set -x
#####################################################################
#created by: Vika Baktov
#purpose: backup and zip partition according to user provided option
#date: 21/10/2020
#version:1.0.0
#####################################################################
#"To use script type one of the following options: a b c d e f
# a : backup MBR partition"	
# b : backup home partition" 	
# c : backup and zip home partition"	
# d : backup and zip root home partition"	
# e : backup and zip the whole disk"
# f : clean up the swap partition"	
#####################################################################

main(){	# the script is running once ---> what if ... i want to do repetetive tasks ?
	local option="$1"	
	local disk_name="$2"	
	local dest_folder="$3"
	local bkp_options='a b'
	local zip_options='c d e'
	if [[ -z $option ]];then
		echo "missing option";usage
	elif [[ " $bkp_options " =~ .*\ $option\ .* ]]; then
			backup_part $option $disk_name $dest_folder;
	elif [[ " $zip_options " =~ .*\ $option\ .* ]]; then
			backup_part $option $disk_name $dest_folder;
			#zip_part $option $disk_name $dest_folder;
	#elif [[ $option=='f' ]];then
			#cleanup_swap_part
	else 
		echo "invalid option"; usage;
	fi		
}
	
backup_part(){	# no validation to type of parameters  --> what if user will mix them up ?
	local option="$1"	
	local disk_name="$2"	
	local dest_folder="$3"
	local bkp_params="bs=2047 count=1"
	$disk_name=$(get_source_for_bkp $disk_name)
	$dest_folder=$(get_dest_of_bkp $dest_folder)
	dd if=$disk_name of=${dest_folder}/$(create_bkp_file_name $option) $bkp_params					
	deco "Finished Backuping"
}

zip_part(){  # Are you sure you can use dd on the folder ?
	local src_folder="$1"	
	local dest_folder="$2"
	local bkp_file="$3"
	#dd if=/dev/sda of=/dev/sdb conv=noerr,sync
	dd if=$src_folder | gzip > $bkp_file.gz	
	deco "Finished Zipping"
}	

#cleanup_swap_part(){
#
#}

get_source_for_bkp(){
	local source_for_bkp=$1
	if [[ -z $disk_name ]];then
		read -p "Please enter disk_name:" disk_name
		#echo $disk_name
	fi
	echo $source_for_bkp
}

get_dest_of_bkp(){
	local dest_of_bkp=$1
	if [[ -z $dest_of_bkp ]];then
		read -p "Please enter destination folder:" dest_of_bkp
		#echo $dest_of_bkp
	fi
	echo $dest_of_bkp
}

create_bkp_file_name(){
	local bkp_option=$1
	case $bkp_option in	
		a) echo MBR_$(date +"%Y%m%d_%T").img;;
		b) echo home_$(date +"%Y%m%d_%T").img;;
		d) echo root_home_$(date +"%Y%m%d_%T").img;;
		e) echo disc_$(date +"%Y%m%d_%T").img;;
	esac
}

usage(){	# GREAT USE OF print formatting - and information passing to the user !!!!
	note="To use script type : $0 a b c d e f"	
	opt_msg=" a : backup MBR partition"	
	opt_msg2=" b : backup home partition" 	
	opt_msg3=" c : backup and zip home partition"	
	opt_msg4=" d : backup and zip root home partition"	
	opt_msg5=" e : backup and zip the whole disk"
	opt_msg6=" f : clean up the swap partition"	
	printf "%s " $note	
	printf "\n$opt_msg" 	
	printf "\n$opt_msg2"	
	printf "\n$opt_msg3"
	printf "\n$opt_msg4"	
	printf "\n$opt_msg5"	
	printf "\n$opt_msg6\n"
	}

deco(){
	_time=2.5
	l="##############################"
	printf "\n$l\n # %s\n$l\n" "$@"
	sleep $_time
	#clear
}

help(){
	deco "Incorrect use of script"
	exit 1
}
#################### !!!!! DO NOT REMOVE !!!!!!!! ###########
main "$@"
